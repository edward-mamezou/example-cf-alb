ビルド手順
=====

# AWS にアーティファクト管理リソースの作成

```
$ aws cloudformation create-stack --region 'ap-northeast-1' --stack-name 'repository' --template-body "`cat aws/repository.yml`"
```

## macOS、Linux の場合

CodeArtifact の認証トークンを取得します。

```
$ export AWS_ACCOUNT=<YOUR AWS ACCOUNT>
$ export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --region ap-northeast-1 --domain example --domain-owner ${AWS_ACCOUNT} --query authorizationToken --output text`
```

OpenAPI 定義から Java コードの生成とモジュールのビルドを実行します。

```
$ docker run -e CODEARTIFACT_AUTH_TOKEN=$CODEARTIFACT_AUTH_TOKEN -e AWS_ACCOUNT=${AWS_ACCOUNT} -v `pwd`:/work -w /work -it --rm maven /bin/bash
# export CI_PROJECT_DIR=`pwd`
# bash build.sh
# mvn -s aws/settings.xml clean install
# exit
```

アプリケーションの Docker イメージをビルドします。

```
$ docker run -e CODEARTIFACT_AUTH_TOKEN=$CODEARTIFACT_AUTH_TOKEN -e AWS_ACCOUNT=${AWS_ACCOUNT} -v /var/run/docker.sock:/var/run/docker.sock -v `pwd`:/work -w /work -it --rm gradle /bin/bash
# export CI_PROJECT_DIR=`pwd`
# cd example
# gradle bootBuildImage
# exit
```

## Windows の場合

WIP

```
$ export AWS_ACCOUNT=<YOUR AWS ACCOUNT>
$ export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --region ap-northeast-1 --domain example --domain-owner ${AWS_ACCOUNT} --query authorizationToken --output text`
```

WIP

```
$ docker run -e CODEARTIFACT_AUTH_TOKEN=$CODEARTIFACT_AUTH_TOKEN -e AWS_ACCOUNT=${AWS_ACCOUNT} -v `pwd`:/work -w /work -it --rm maven /bin/bash
# export CI_PROJECT_DIR=`pwd`
# bash build.sh
# mvn -s aws/settings.xml clean install
# exit
```

```
> set CI_PROJECT_DIR=%CD%
> cd example
> gradlew bootBuildImage
```

## ローカルPC での動作確認

```
$ docker run -p 8080:8080 -it --rm example:0.1.0-SNAPSHOT
```

http://localhost:8080/test

ビルドしたイメージを ECR に登録

```
$ export AWS_ACCOUNT=<YOUR AWS ACCOUNT ID>
$ aws ecr get-login-password --region ap-northeast-1 | docker login --username AWS --password-stdin ${AWS_ACCOUNT}.dkr.ecr.ap-northeast-1.amazonaws.com
$ docker tag example:0.1.0-SNAPSHOT ${AWS_ACCOUNT}.dkr.ecr.ap-northeast-1.amazonaws.com/example:0.1.0-SNAPSHOT
$ docker push ${AWS_ACCOUNT}.dkr.ecr.ap-northeast-1.amazonaws.com/example:0.1.0-SNAPSHOT
```

## GitLab CI/CD

GitLab CI/CD パイプラインでは、下の表の変数を設定する必要があります。

| 変数名 (Variables) | 説明 |
|:---|:---|
| AWS_ACCOUNT | AWS ACCOUNT の ID |
| AWS_ACCESS_KEY_ID | アクセスキー |
| AWS_SECRET_ACCESS_KEY | シークレット |

ECR にイメージを登録するところまでは、GitLab CI/CD により継続的にデプロイされます。

# AWS Fargate での動作確認

まず、ネットワーク (VPC、Internet Gateway、ルートテーブル などを構成)

```
$ aws cloudformation create-stack --region 'ap-northeast-3' --stack-name 'network' --template-body "`cat aws/network.yml`"
```

```
$ export CERTIFICATES=<証明書のARN>
$ export HOSTED_ZONE_ID=<Route 53 のゾーン名>
$ export HOST_NAME=<ELBのホスト名>
$ aws cloudformation create-stack --region 'ap-northeast-3' --stack-name 'deploy' --template-body "`cat aws/deploy.yml`" --capabilities CAPABILITY_IAM --parameters ParameterKey=CertificatesForLoadBalancer,ParameterValue=${CERTIFICATES} ParameterKey=LoadBalancerHostName,ParameterValue=${HOST_NAME} ParameterKey=HostedZoneId,ParameterValue=${HOSTED_ZONE_ID}
```
